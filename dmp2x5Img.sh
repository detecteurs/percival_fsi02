# use example: ./dmp2x5Img.sh aaa
# will tcpdump-save aaa_lnk0 and aaa_lnk1 , containging 2x5Img
tcpdump greater 448 -B1800000000 -i p3p1 -c 8480 -w $1_lnk0.dmp & tcpdump greater 448 -B1800000000 -i p3p2 -c 8480 -w $1_lnk1.dmp && fg
