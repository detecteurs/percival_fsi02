#! /usr/bin/bash

percival-hl-system-command -c stop_acquisition
percival-hl-system-command -c exit_acquisition_armed_status

if [ "$#" -ne 1 ]; then
   echo "Not changing I_Bias/Vref/PowerSupply";
else
    echo "change biases from ${1} to 08_1_ready"
    percival-hl-scan-setpoints -i ${1} -f 08_1_ready -n 2 -d 500
fi

echo "Load ADC25MHz, PLL120MHz, VRST, PGAB SequentialMode, 10 Images, 12ms integration"
percival-hl-configure-clock-settings -i ./DESY/FSI02/config/01_Clock_Settings/ClockSettings_N22_125MHz_ACD25MHz.ini
percival-hl-configure-chip-readout-settings -i ./DESY/FSI02/config/02_Chip_Readout_Settings/ChipReadoutSettings_N15_VRST_ADC25MHz_PLL120MHz_SEQUENTIAL.ini
percival-hl-configure-system-settings -i ./DESY/FSI02/config/03_System_Settings/SystemSettings_N15_pixel_10Img_12ms_SEQUENTIAL.ini

echo RESET DATA SYNCH STATUS x 2...
# EXIT ARMED STATUS
percival-hl-system-command -c exit_acquisition_armed_status
# ASSERT CPNI FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_002_SET_CPNI.ini
# TOGGLE CPNI_EXT
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 2
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 0
# DEASSERT ALL FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_000_SAFE_START.ini
# ENTER ARMED STATUS
percival-hl-system-command -c enter_acquisition_armed_status

# EXIT ARMED STATUS
percival-hl-system-command -c exit_acquisition_armed_status
# ASSERT CPNI FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_002_SET_CPNI.ini
# TOGGLE CPNI_EXT
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 2
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 0
# DEASSERT ALL FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_000_SAFE_START.ini
# ENTER ARMED STATUS
percival-hl-system-command -c enter_acquisition_armed_status

echo  "-----DONE-----"
echo  "SeqMod, dmuxSELswitching"
echo  "Mode: VRST"
