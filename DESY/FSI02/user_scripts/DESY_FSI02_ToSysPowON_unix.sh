#! /usr/bin/bash

if [ "$#" -ne 1 ]; then
   echo "ERROR!!! put which setpoint you are in now as arg"
   echo "08_1_ready, 08_FSI02_3Ghml, 08_FSI02_3Th, 08_FSI02_3Gvml, 08_FSI02_3Tv, 08_FSI02_3Tm, 08_FSI02_3Tl"
   exit 2
fi

percival-hl-system-command -c stop_acquisition
percival-hl-system-command -c exit_acquisition_armed_status

echo "change biases from ${1} to 08_FSI02_3Tl"
percival-hl-scan-setpoints -i ${1} -f 08_1_ready -n 2 -d 500

echo Load default operating status
percival-hl-configure-clock-settings -i ./DESY/FSI02/config/01_Clock_Settings/ClockSettings_N22_125MHz_ACD25MHz.ini
percival-hl-configure-chip-readout-settings -i ./DESY/FSI02/config/02_Chip_Readout_Settings/ChipReadoutSettings_N05_3T_120MHz.ini
percival-hl-configure-system-settings -i ./DESY/FSI02/config/03_System_Settings/SystemSettings_N05_pixel_Test.ini

echo RESET DATA SYNCH STATUS x 2...
# EXIT ARMED STATUS
percival-hl-system-command -c exit_acquisition_armed_status
# ASSERT CPNI FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_002_SET_CPNI.ini
# TOGGLE CPNI_EXT
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 2
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 0
# DEASSERT ALL FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_000_SAFE_START.ini

# ASSERT CPNI FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_002_SET_CPNI.ini
# TOGGLE CPNI_EXT
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 2
percival-hl-set-system-setting -s ADVANCED_CPNI_EXT_options -v 1
percival-hl-set-system-setting -s ADVANCED_Enable_CPNI_EXT -v 0
# DEASSERT ALL FLAGS IN DEBUG REGISTERS
percival-hl-configure-sensor-debug -i ./DESY/FSI02/config/04_Sensor_Settings/SensorDebug_000_SAFE_START.ini
# ENTER ARMED STATUS
percival-hl-system-command -c enter_acquisition_armed_status
echo  "DONE"
echo  "Sys-after-PowON"
