echo PERCIVAL POWERUP STARTED / DESY
echo Applies to detector head with sensor Wafer 3,Chip 3
echo

echo - Downloading device settings...
percival-hl-download-channel-settings
percival-hl-system-command -c enable_global_monitoring

echo - Loading initial safe status...
percival-hl-configure-clock-settings -i ./DESY/FSI02/config/01_Clock_Settings/ClockSettings_N00_SAFE_START.ini
percival-hl-configure-chip-readout-settings -i ./DESY/FSI02/config/02_Chip_Readout_Settings/ChipReadoutSettings_N00_SAFEstart.ini
percival-hl-configure-system-settings -i ./DESY/FSI02/config/03_System_Settings/SystemSettings_N00_SAFE_START.ini

echo - Preparing powerboard...
percival-hl-system-command -c disable_LVDS_IOs
percival-hl-system-command -c stop_acquisition
percival-hl-system-command -c exit_acquisition_armed_status
percival-hl-system-command -c fast_disable_control_standby
percival-hl-system-command -c disable_startup_mode
percival-hl-initialise-channels
percival-hl-system-command -c fast_sensor_powerdown

echo - Initializing...
percival-hl-system-command -c disable_safety_actions
percival-hl-system-command -c enable_device_level_safety_controls
percival-hl-system-command -c enable_system_level_safety_controls
percival-hl-system-command -c enable_experimental_level_safety_controls
percival-hl-configure-control-groups -i ./DESY/FSI02/config/05_Spreadsheets/DESY_FSI02_Group_Definitions.xls
percival-hl-configure-monitor-groups -i ./DESY/FSI02/config/05_Spreadsheets/DESY_FSI02_Group_Definitions.xls
percival-hl-configure-setpoints -i ./DESY/FSI02/config/05_Spreadsheets/DESY_FSI02_Setpoint_Definitions.xls

